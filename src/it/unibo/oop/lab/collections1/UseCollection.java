package it.unibo.oop.lab.collections1;

import java.util.*;
import java.util.Map;

/**
 * Example class using {@link List} and {@link Map}.
 * 
 */
public final class UseCollection {

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
    	
    	List<Integer> arrayList =  new ArrayList<>();
    	
    	for(int i = 1000; i< 2000; i++)
    	{    		
    		arrayList.add(i);
    	}
    	
    	List<Integer> linkList = new LinkedList<>(arrayList);
    	
    	int tempvar = arrayList.get(0);
    	arrayList.set(0, arrayList.get(arrayList.size()-1));
    	arrayList.set(arrayList.size()-1, tempvar);
    	
    	for(Integer i: arrayList) {
    		System.out.println(i);    		
    	}
    	
    	long time = System.nanoTime();
    	
    	for(int i=0; i<100000; i++) {
    		arrayList.add(0, i);   		
    	}
    	
    	System.out.println("\nLink Tempo: "+ (System.nanoTime() - time));
    	
    	time = System.nanoTime();
    	
    	for(int i=0; i<100000; i++) {
    		linkList.add(0, i);   		
    	}
    	
    	System.out.println("\nLink Tempo: "+ (System.nanoTime() - time));
    	
    	time = System.nanoTime();
    	
    	for(int i = 0; i<1000; i++) {
    		arrayList.get(arrayList.size()/2);
    	}
    	
    	System.out.println("\nLink Tempo: "+ (System.nanoTime() - time));
    	
    	time = System.nanoTime();
    	
    	for(int i = 0; i<1000; i++) {
    		linkList.get(arrayList.size()/2);
    	}  	
    	
    	System.out.println("\nLink Tempo: "+ (System.nanoTime() - time));
    	    		 
        final Map<String, Long> mapworld= new HashMap<>();
        
        mapworld.put("Africa", 1110635000L);
        mapworld.put("Americas", 972005000L);
        mapworld.put("Antartica", 0L);
        mapworld.put("Asia", 4298723000L);
        mapworld.put("Europe", 742452000L);
        mapworld.put("Oceania", 38304000L);     
        
        List<Long> pop= new ArrayList<>(mapworld.values());
        
        long popTot=0;
        
        for(Long l: pop) {
        	popTot = popTot+l;
        }
        
        System.out.println("Popolazione: "+popTot);
        
        /*
         * 8) Compute the population of the world
         */
    }
}
